;;; my-utils.el --- Some utils functions

;; Author: Jacquin Théo <théo@jacqu.in>

;;; Commentary:
;; Some stuff

;;; Code:
(defun show-timestamp (timestamp)
  "Display the formatted string of input TIMESTAMP."
  (interactive (list
		(read-string
		 (format "Timestamp (%s):" (thing-at-point 'word))
		 nil nil (thing-at-point 'word))))
  (message "%s"
	   (format-time-string "%F %H:%M:%S" (string-to-number timestamp))))

(provide 'my-utils)
;;; my-utils.el ends here
