;;; cycleNomenclatureMove.el --- Cycle around sub/superword

;; Author: Jacquin Théo <théo@jacqu.in>

;;; Commentary:
;; Help moving around variables using differant nomenclature by  leveraging
;; subword and superword Emacs' mode and allowing cycling through them

;;; Code:

(defun cycle-nomenclature ()
  "Cycle between normal, superword and subword mode."
  (interactive)
  (cond
   ((bound-and-true-p superword-mode) (subword-mode t))
   ((bound-and-true-p subword-mode) (subword-mode -1))
   (t (superword-mode t))))

(provide 'cycle-nomenclature-move)
;;; cycle-nomenclature-move.el ends here
