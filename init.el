;;; Author: Théo Jacquin <theo@jacqu.in>


;;; Prepare package initialization

(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives
      '(("GNU ELPA"     . "https://elpa.gnu.org/packages/")
        ("MELPA Stable" . "https://stable.melpa.org/packages/")
        ("MELPA"        . "https://melpa.org/packages/"))
      package-archive-priorities
      '(("MELPA Stable" . 10)
	("GNU ELPA"     . 5)
	("MELPA"        . 0)))
(package-initialize)

;; Bootstrap use-package
(unless (package-installed-p 'use-package)
   (package-refresh-contents)
   (package-install 'use-package))

;; Bootstrap local packages
(add-to-list 'load-path (expand-file-name "local-extensions" user-emacs-directory))
(require 'my-utils)

;;; Global emacs conf

;; Move customizations elsewhere and don't load them
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

;; Remove useless X stuff
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

;; Transparency
(set-frame-parameter (selected-frame) 'alpha '(97 . 96))
(add-to-list 'default-frame-alist '(alpha . (97 . 96)))

;; Unbind sleep Button
(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "C-x C-z"))

;; Save pasted stuff from other program when killing
(setq save-interprogram-paste-before-kill t)

;; Uniformize yes-or-no prompt
(defalias 'yes-or-no-p 'y-or-n-p)

;; Displays text as I want
(setq indicate-empty-lines t)

;; Improvments for azerty layout
(global-set-key (kbd "M-ù") 'query-replace)
(global-set-key (kbd "C-z") 'set-mark-command)

;; Theme
(load-theme 'wombat)

;;; Packages

;; Needed for use-package conf
(use-package diminish
  :ensure t)

;; Visual improvements
(use-package hl-line
  :config
  (setq hl-line-sticky-flag nil)
  (set-face-attribute
   'hl-line nil
   :inherit nil
   :background "#292929")
  :init (global-hl-line-mode 1))

(use-package whitespace
  :diminish
  :hook (prog-mode . whitespace-mode)
  :init
  (setq whitespace-line-column 80
	whitespace-style '(face lines-tail trailing)))

(use-package golden-ratio
  :ensure t
  :diminish
  :after switch-window
  :hook switch-window-finish
  :init
  (golden-ratio-mode 1))

(use-package switch-window
  :ensure t
  :init
  (global-set-key (kbd "C-x o") 'switch-window)
  (global-set-key (kbd "C-x 1") 'switch-window-then-maximize)
  (global-set-key (kbd "C-x 2") 'switch-window-then-split-below)
  (global-set-key (kbd "C-x 3") 'switch-window-then-split-right)
  (global-set-key (kbd "C-x 0") 'switch-window-then-delete)

  (global-set-key (kbd "C-x 4 d") 'switch-window-then-dired)
  (global-set-key (kbd "C-x 4 f") 'switch-window-then-find-file)
  (global-set-key (kbd "C-x 4 m") 'switch-window-then-compose-mail)
  (global-set-key (kbd "C-x 4 r") 'switch-window-then-find-file-read-only)
  (global-set-key (kbd "C-x 4 s") 'switch-window-then-swap-buffer)
  (global-set-key (kbd "C-x 4 b") 'switch-window-then-display-buffer)

  (global-set-key (kbd "C-x 4 0") 'switch-window-then-kill-buffer))

(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))

;; Efficiency helpers
(use-package magit
  :ensure t
  :bind (("C-c g" . magit-status)
	 ("C-c C-g l" . magit-file-log)
	 ("C-c f" . magit-grep)
	 ("C-c C-g b" . magit-blame)))

(use-package helm
  :ensure t
  :bind (("M-x" . helm-M-x)
	 ("C-x C-f" . helm-find-files)
	 ("C-x b" . helm-buffers-list)
	 :map helm-map
	 ("<tab>" . helm-execute-persistent-action)))

(use-package helm-ring
  :after helm
  :init
  (defun my-helm-show-kill-ring ()
    "Like `helm-show-kill-ring' but preselect the second newest."
    (interactive)
    (setq helm-kill-ring--truncated-flag nil)
    (let ((enable-recursive-minibuffers t))
      (helm :sources helm-source-kill-ring
	    :preselect (current-kill 1 t)
	    :buffer "*helm kill ring*"
	    :resume 'noresume
	    :allow-nest t)))
  (global-set-key (kbd "M-y") 'my-helm-show-kill-ring))

(use-package org
  :ensure t
  :bind (("C-c c" . org-capture)
	 ("C-c a" . org-agenda))
  :config
  (setq org-capture-templates
	'(("t" "Todo [inbox]" entry (file "~/gtd/inbox.org") "* TODO %i%?")
	  ("T" "Tickler" entry (file "~/gtd/tickler.org") "* %i%? \n %U")
	  ("l" "Link to read" entry (file "~/gtd/links.org") "* TODO %i%? \n %U")
	  ("b" "Bépo" table-line (file+headline "~/gtd/bépo.org" "Progression") "| %U | %? | |"))
	org-refile-use-outline-path
	'file
	org-refile-targets
	'(("~/gtd/gtd.org" :maxlevel . 3)
	  ("~/gtd/someday.org" :level . 1)
	  ("~/gtd/tickler.org" :maxlevel . 2))
	org-todo-keywords
	'((sequence "TODO" "NEXT" "BLOCKED" "|" "DONE" "DELEGATED" "CANCELLED")))

  (setq org-agenda-files '("~/gtd/"))

  ;; Hack org-mode latex export to ignore headline if specified
  (defun org-latex-ignore-heading-filter-headline (headline backend info)
    "Strip headline from HEADLINE. Ignore BACKEND and INFO."
    (when (and (org-export-derived-backend-p backend 'latex)
	       (string-match "\\`.*ignoreheading.*\n" headline))
      (replace-match "" nil nil headline)))
  (require 'ox-latex)
  (add-to-list 'org-export-filter-headline-functions
	       'org-latex-ignore-heading-filter-headline))


(use-package keyfreq
  :ensure t
  :init
  (keyfreq-mode 1)
  (keyfreq-autosave-mode 1)
  (setq keyfreq-file (expand-file-name "keyfreq" user-emacs-directory)
        keyfreq-file-lock (expand-file-name "keyfreq.lock" user-emacs-directory)))

(use-package cycle-nomenclature-move
  ; local package
  :bind (("C-!" . cycle-nomenclature)))

(use-package multiple-cursors
  :ensure t
  :bind (("C-c e" . mc/edit-lines)
		 ("C->" . mc/mark-next-like-this-word)
		 ("C-<" . mc/mark-previous-like-this-word)))

;; Programming mode

(global-set-key (kbd "C-c b") 'ff-find-other-file)

(use-package gnuplot :ensure t)

(use-package lua-mode :ensure t)

(use-package ess :ensure t)

(use-package dockerfile-mode :ensure t)

(use-package web-mode
  :ensure t
  :mode (("\\.html\\'" . web-mode)
         ("\\.html\\.erb\\'" . web-mode)
         ("\\.mustache\\'" . web-mode)
         ("\\.jinja\\'" . web-mode)
	 ("\\.j2\\'" . web-mode))
  :config
  (progn
    (setq web-mode-engines-alist
	  '(("\\.jinja\\'" . "django"))
	  web-mode-script-padding 2)))

(use-package php-mode
  :ensure t
  :mode "\\.php\\'")

;; Javascript mode config
(setq js-indent-level 4)
(setq js-switch-indent-offset 4)
(defun my-js-mode-hook ()
  (setq indent-tabs-mode nil))

(add-hook 'js-mode-hook 'my-js-mode-hook)

;;; Flycheck conf
(use-package flycheck
  :ensure t
  :diminish
  :init (global-flycheck-mode))

;; JS
(defun my-eslint-mode-hook ()
  (let* ((root (locate-dominating-file
                (or (buffer-file-name) default-directory)
                "node_modules"))
         (eslint (and root
                      (expand-file-name "node_modules/eslint/bin/eslint.js"
                                        root))))
    (when (and eslint (file-executable-p eslint))
      (setq-local flycheck-javascript-eslint-executable eslint))))

(add-hook 'js-mode-hook 'my-eslint-mode-hook)

;; PHP
(defun my-php-mode-hook ()
  "My PHP-mode hook."
  (flycheck-mode t)
  (c-set-offset 'case-label '+))

(add-hook 'php-mode-hook 'my-php-mode-hook)

;; C
(setq-default indent-tabs-mode nil)
(setq c-default-style "k&r"
      c-basic-offset 4
      tab-width 4)
(c-set-offset 'case-label '+)
;; Tests
